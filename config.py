import os
import random


class Config(object):
    # SECURITY: Insecure randomization
    # https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/random/rule-random.yml
    defaultKey = random.choice('0123456789abcdef')
    SECRET_KEY = os.environ.get('NOTES_SECRET_KEY') or defaultKey