import os
import socket
import requests

from notes import db, note, auth, users
from notes.forms import AddForm, AdminForm, ResetForm, DeleteForm
from flask import render_template, request, flash, redirect, jsonify, send_file
from werkzeug.security import check_password_hash


@note.route('/', methods=['GET', 'POST'])
@note.route('/index', methods=['GET', 'POST'])
def index():
    conn = db.create_connection()

    # For running locally
    port = os.environ.get("PORT", 5000)
    ing_path = "http://127.0.0.1:" + str(port)
    # For running on Kubernetes
    if os.environ.get("NOTES_ING_PATH"):
        ing_path = "/" + os.environ.get("NOTES_ING_PATH")
    
    items = []
    try:
        items = db.select_all_notes(conn, False)
    except Exception as e:
        flash('Error generating notes: Check Logs')
        note.logger.error("Error Generating Notes: %s" % e)

    arr = []
    if len(items) > 0:
        for item in items:
            try:
                _id = item[0]
                _note = item[1]
                note_str = '%s | %s' % (_id, _note)
                arr.append(note_str)
            except Exception as e:
                note.logger.error(e)

    add_form = AddForm()
    if add_form.validate_on_submit():
        try:
            result = add_note(add_form.note_field.data)

            if result[1] == 200:
                flash('Note "{}" has been added!'.format(
                    add_form.note_field.data))
            else:
                flash('Failed to add Note "{}": {}'.format(
                    add_form.note_field.data, "Check Logs"))
        except Exception as e:
            flash('Failed to add Note "{}": {}'.format(
                add_form.note_field.data, e))

        return redirect(ing_path)

    delete_form = DeleteForm()
    if delete_form.validate_on_submit():
        try:
            result = delete_note(delete_form.id_field.data)
            if result[1] == 204:
                flash('Note "#{}" has been Deleted!'.format(
                    delete_form.id_field.data))
            else:
                flash('Failed to delete Note with id "{}": {}'.format(
                    delete_form.id_field.data, "Check Logs"))
        except Exception as e:
            flash('Failed to delete Note with id "{}": {}'.format(
                delete_form.id_field.data, e))

        return redirect(ing_path)

    admin_form = AdminForm()
    if admin_form.validate_on_submit():
        return redirect(ing_path + '/admin')
    
    return render_template('index.html',
                            notes=arr,
                            add_form=add_form,
                            delete_form=delete_form,
                            admin_form=admin_form)

@note.route('/admin', methods=['GET', 'POST'])
@auth.login_required
def admin():
    conn = db.create_connection()
    
    # For running locally
    port = os.environ.get("PORT", 5000)
    ing_path = "http://127.0.0.1:" + str(port)
    # For running on Kubernetes
    if os.environ.get("NOTES_ING_PATH"):
        ing_path = "/" + os.environ.get("NOTES_ING_PATH")

    items = []
    try:
        items = db.select_all_notes(conn, True)
    except Exception as e:
        flash('Error generating notes: Check Logs')
        note.logger.error("Error generating notes: %s" % e)

    arr = []
    if len(items) > 0:
        for item in items:
            try:
                _id = item[0]
                _note = item[1]
                _secret = str(item[2])

                note_str = '| %s | %s | %s |' % (_id, _note, _secret)
                arr.append(note_str)
            except Exception as e:
                note.logger.error(e)

    add_form = AddForm()
    if add_form.validate_on_submit():
        try:
            result = add_note_admin(add_form.note_field.data)

            if result[1] == 200:
                flash('Note "{}" has been added!'.format(
                    add_form.note_field.data))
            else:
                flash('Failed to add Note "{}": {}'.format(
                    add_form.note_field.data, "Check Logs"))
        except Exception as e:
            flash('Failed to add Note "{}": {}'.format(
                add_form.note_field.data, e))
        
        return redirect(ing_path + '/admin')

    delete_form = DeleteForm()
    if delete_form.validate_on_submit():
        try:
            result = delete_note_admin(delete_form.id_field.data)

            if result[1] == 204:
                flash('Note with id "{}" has been Deleted!'.format(
                    delete_form.id_field.data))
            else:
                flash('Failed to delete Note with id "{}": {}'.format(
                    delete_form.id_field.data, "Check Logs"))
        except Exception as e:
            flash('Failed to delete Note with id "{}": {}'.format(
                delete_form.id_field.data, e))

        return redirect(ing_path + '/admin')

    reset_form = ResetForm()
    if reset_form.validate_on_submit():
        try:
            result = reset()
            if result:
                flash('Database Table "{}" has been reset!'.format("notes"))
            else:
                flash('Database Table "{}" Failed to reset: Check Logs'.format("notes"))
        except Exception as e:
            flash('Database Table "{}" Failed to reset: {}}'.format("notes", e))
        
        return redirect(ing_path + '/admin')
    
    return render_template('admin.html',
                            notes=arr,
                            add_form=add_form,
                            delete_form=delete_form,
                            reset_form=reset_form)

@auth.verify_password
def verify_password(username, password):
    if username in users and \
            check_password_hash(users.get(username), password):
        return username

@note.route('/api', methods=['POST'])
def add_note(msg="", admin=False):
    if not msg:
        data = request.get_json(force=True)
        msg = data.get('message')

    if not msg:
         return jsonify({"Error": "No message in Request"}), 400

    if len(msg) > 100:
         # SECURITY: 500s are reported by the Web API Fuzzer
         # https://docs.gitlab.com/ee/user/application_security/api_fuzzing/
         return jsonify({"Error": "Message too long, keep at chars or less"}), 500

    if (msg == "\""):
        response = jsonify({"Success": "Maybe a Security Issue!"}), 200
        response.headers.set('Content-Type', 'text/html')
        return response

    conn = db.create_connection()

    try:
        note.logger.info("Attempting to add note with msg: {}".format(msg))
        db.create_note(conn, msg, admin)
        return jsonify({"Success": "Note added!"}), 200
    except Exception as e:
        err = "%s" % e
        return jsonify({"Error": err}), 500

@note.route('/api/admin', methods=['POST'])
@auth.login_required
def add_note_admin(msg=""):
    return add_note(msg=msg, admin=True)

@note.route('/api', methods=['GET'])
def get_note(id=None, admin=False):
    id = request.args.get('id')
    conn = db.create_connection()

    if id:
        try:
            result = str(db.select_note_by_id(conn, id, admin))
            return jsonify({"Note": result}), 200
        except Exception as e:
            note.logger.error("Error Getting Note: %s" % e)
            return jsonify({"Error": e}), 500
    
    try:
        result = str(db.select_all_notes(conn, admin))
        return jsonify({"Notes": result}), 200
    except Exception as e:
        note.logger.error("Error Getting Notes: %s" % e)
        return jsonify({"Error": e}), 500

@note.route('/api/admin', methods=['GET'])
@auth.login_required
def get_note_admin(id=None):
    id = request.args.get('id')
    return get_note(id, admin=True)

@note.route('/api', methods=['DELETE'])
def delete_note(id=None, admin=False):
    if id is None:
        id = request.args.get('id')
        if id is None:
            return jsonify({"Error": "No id sent in request!"}), 400

    # Check if item exists
    conn = db.create_connection()
    items = []
    try:
        items = db.select_note_by_id(conn, str(id), admin)
        note.logger.info(str(items))
    except Exception as e:
        note.logger.error("Error Checking Note: %s" % e)
        return jsonify({"Error": str(e)}), 500

    if len(items) == 0:
        return jsonify({"Error": ("Note with id '%s' not found" % id) }), 400

    # Delete the note
    conn2 = db.create_connection()
    try:
        db.delete_note(conn2, str(id), admin)
    except Exception as e:
        note.logger.error("Error Deleting Note: %s" % e)
        return jsonify({"Error": str(e)}), 500

    # Verify if item was deleted
    conn3 = db.create_connection()
    items = []
    try:
        items = db.select_note_by_id(conn3, id, admin)
        note.logger.info(str(items))
    except Exception as e:
        note.logger.error("Error Verifying note was deleted: %s" % e)
        return jsonify({"Error": str(e)}), 500

    if len(items) != 0:
        note.logger.error("Error Note still exists")
        return jsonify({"Error": "Note still exists"}), 500

    return jsonify({"Success": "Note Deleted!"}), 204

@note.route('/api/admin', methods=['DELETE'])
@auth.login_required
def delete_note_admin(id=None):
    return delete_note(id, admin=True)

def reset():
    conn = db.create_connection()
    query = """DROP TABLE notes;"""

    try:
        c = conn.cursor()
        c.execute(query)
    except Exception as e:
        note.logger.error("Failed to drop database table 'notes': %s" % e)
        conn.close()
        return False

    try:
        db.create_table(conn)
    except Exception as e:
        note.logger.error("Failed to re-create database table 'notes': %s" % e)
        return False

    return True

# Adds the vulnerable db call path to the API
@note.route('/api/vuln', methods=['GET'])
def get_note_with_vulnerability(id=None, admin=False):
    id = request.args.get('id')
    conn = db.create_connection()

    try:
        result = str(db.select_notes_injection_prone(conn, id, admin))
        return jsonify({"Note": result}), 200
    except Exception as e:
        note.logger.error("Error Getting Notes: %s" % e)
        return jsonify({"Error": e}), 500

# SECURITY: Insecure Deserialization and Injection
# https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/exec/rule-import_subprocess.yml
# https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/exec/rule-subprocess_call.yml
@note.route('/api/pwd', methods=['GET'])
def get_current_directory():
    import subprocess
    command = 'pwd'
    pwd = str(subprocess.call(command, shell=0))

    return pwd

# SECURITY: Bad file permissions
# https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/file_permissions/rule-general_bad_permission.yml
@note.route('/api/passwords', methods=['GET'])
def get_credentials():
    f = open("credentials.txt", "w")

    if not users:
        return jsonify({"Error": "No Users Present"}), 400
    
    for user in users:
        password_hash = users.get(user)
        f.write("username: %s, hash: %s" % (user, password_hash))
    
    f.close()
    os.chmod("credentials.txt", 0o777)

    try:
        return send_file(f.name, attachment_filename='credentials.txt'), 200
    except Exception as e:
        note.logger.error("Failed to send the 'credentials.txt' file: %s" % e)
        return jsonify({"Error": str(e)}), 500